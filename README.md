The app 
this base follow most best practices of architecture pattern as following diagram:
https://developer.android.com/codelabs/kotlin-android-training-repository#5

consist of 3 layers(Data layer/ DI / UI)

   # data layer
#### Here is our data layer which responsible for send and fetch data from Api , database and shared preference

1. In `Model` package write your `POGOs` model inside it

2. In `remote` inside `GitHubApis` package write your abstract method for retrofit call and Create name for each stage 

3. In `ViewState` inside sealed class detect the state of request 

4. In `repository` inside dummy functions as documentation and multi implement and for unit tes

5. in `cache` inside `PreferencesGateway` generic clean class to simplify using prefrences



# UI
based on mvvm 


# BuildSrc module
   this module to easy mainpulate with our most common dependency and navigate it with centerliaze file without hedeache




   # DI  
#### Here is our data module which responsible for create inistance as singleton class and provide sources 


