package com.rowaad.rxbase.ui.posts.viewmodel

import com.rowaad.rxbase.data_layer.model.PostModel
import com.rowaad.rxbase.data_layer.model.PostModelItem
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody

fun getFakePosts(): PostModel {
    val model= PostModel()
    model.addAll(listOf(PostModelItem(body = "fake1"), PostModelItem(body = "fake2"), PostModelItem(body = "fake3")))
    return model
}
fun errorResponseBody(): ResponseBody {
    return "{\"key\":[\"somestuff\"]}"
        .toResponseBody("application/json".toMediaTypeOrNull())
}