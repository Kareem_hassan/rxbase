package com.rowaad.rxbase.ui.posts.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.rowaad.rxbase.data_layer.model.PostModel
import com.rowaad.rxbase.data_layer.remote.Fail
import com.rowaad.rxbase.data_layer.remote.Loading
import com.rowaad.rxbase.data_layer.remote.Success
import com.rowaad.rxbase.data_layer.remote.ViewState
import com.rowaad.rxbase.data_layer.repository.github.GitHubRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.Response

class MainViewModelTest {
    @get:Rule
     val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    lateinit var repository: GitHubRepository

    @Mock
    private lateinit var observerState:Observer<ViewState<Any>>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<ViewState<Any>>


    lateinit var mainViewModel: MainViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setIoSchedulerHandler{ Schedulers.trampoline()}
        mainViewModel=MainViewModel(repository)
    }


    @Test
    fun `getGitHubPosts() with no parms then complete`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        val test = repository.getPosts().test()
        test.assertComplete()
    }

    @Test
    fun `getGitHubPosts() with no parms then invoked state 3 times`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
    }

    @Test
    fun `getGitHubPosts() with no parms then loading invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        assertTrue(argumentCaptor.allValues[0] is Loading)

    }
    @Test
    fun `getGitHubPosts() with no parms then loading(true) invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[0] as Loading
        assertTrue(loading.isVisible)

    }

    @Test
    fun `getGitHubPosts() with no parms then success invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        assertTrue(argumentCaptor.allValues[1] is Success)

    }

    @Test
    fun `getGitHubPosts() with no parms then return data`() {
        val fakePosts = getFakePosts()
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(fakePosts)))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val successCase = argumentCaptor.allValues[1] as Success
        assertEquals(successCase.data as PostModel ,fakePosts)
    }

    @Test
    fun `getGitHubPosts() with no parms then loading(false) invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.success(getFakePosts())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[2] as Loading
        assertTrue(loading.isVisible.not())
    }

    //general error scenario
    @Test
    fun `getGitHubPosts() with no parms then general error not complete`() {
        `when`(repository.getPosts()).thenReturn(Observable.error(Throwable()))
        val test = repository.getPosts().test()
        test.assertNotComplete()
    }

    @Test
    fun `getGitHubPosts() with no parms then general error invoked state 3 times`() {
        `when`(repository.getPosts()).thenReturn(Observable.error(Throwable()))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
    }

    @Test
    fun `getGitHubPosts() with no parms then general error loading(true) invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.error(Throwable()))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[0] as Loading
        assertTrue(loading.isVisible)

    }

    @Test
    fun `getGitHubPosts() with no parms then general error invoked Fail`() {
        `when`(repository.getPosts()).thenReturn(Observable.error(Throwable()))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        assertTrue(argumentCaptor.allValues[1] is Fail)
    }

    @Test
    fun `getGitHubPosts() with no parms then general error return Throwable`() {
        val throwable = Throwable()
        `when`(repository.getPosts()).thenReturn(Observable.error(throwable))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val fail = argumentCaptor.allValues[1] as Fail
        assertEquals(fail.error,throwable)
    }

    @Test
    fun `getGitHubPosts() with no parms then general error occurred then loading(false) invoked`() {
        val throwable = Throwable()
        `when`(repository.getPosts()).thenReturn(Observable.error(throwable))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[2] as Loading
        assertTrue(loading.isVisible.not())
    }

    // error body scenario
    @Test
    fun `getGitHubPosts() with no parms then error body invoke complete`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody())))
        val test = repository.getPosts().test()
        test.assertComplete()
    }

    @Test
    fun `getGitHubPosts() with no parms then error body liveData invoked state 3 times`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
    }

    @Test
    fun `getGitHubPosts() with no parms then error body loading(true) invoked`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[0] as Loading
        assertTrue(loading.isVisible)
    }


    @Test
    fun `getGitHubPosts() with no parms then error body invoked Fail`() {
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody())))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        assertTrue(argumentCaptor.allValues[1] is Fail)
    }

    @Test
    fun `getGitHubPosts() with no parms then error body return error of response body`() {
        val errorResponseBody = errorResponseBody()
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody)))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val fail = argumentCaptor.allValues[1] as Fail
        assertEquals(fail.error as ResponseBody , errorResponseBody)
    }
    @Test
    fun `getGitHubPosts() with no parms then error body occurred then loading(false) invoked`() {
        val errorResponseBody = errorResponseBody()
        `when`(repository.getPosts()).thenReturn(Observable.just(Response.error(400, errorResponseBody)))
        mainViewModel.viewState.observeForever(observerState)
        mainViewModel.getGitHubPosts()
        verify(observerState, times(3)).onChanged(argumentCaptor.capture())
        val loading = argumentCaptor.allValues[2] as Loading
        assertTrue(loading.isVisible.not())
    }
}