package com.rowaad.rxbase.data_layer.repository.github

import com.rowaad.rxbase.data_layer.model.PostModel
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response


interface GitHubRepository {
 fun getPosts(): Observable<Response<PostModel>>
}