package com.rowaad.rxbase.data_layer.remote

import com.rowaad.rxbase.data_layer.model.PostModel
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import retrofit2.http.GET

interface GitHubApis {
    @GET("posts")
    fun getPosts(): Observable<Response<PostModel>>
}
