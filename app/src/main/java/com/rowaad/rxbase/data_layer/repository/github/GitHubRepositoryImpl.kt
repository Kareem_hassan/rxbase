package com.rowaad.rxbase.data_layer.repository.github

import com.rowaad.rxbase.data_layer.cache.PreferencesGateway
import com.rowaad.rxbase.data_layer.model.PostModel
import com.rowaad.rxbase.data_layer.remote.GitHubApis
import com.rowaad.rxbase.data_layer.repository.github.GitHubRepository
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import javax.inject.Inject

class GitHubRepositoryImpl  @Inject constructor(
    private val githubApi: GitHubApis,
    private val preferences: PreferencesGateway
): GitHubRepository {
    override  fun getPosts(): Observable<Response<PostModel>> {
        return githubApi.getPosts()
    }

}