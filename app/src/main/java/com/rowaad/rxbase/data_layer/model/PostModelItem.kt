package com.rowaad.rxbase.data_layer.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PostModelItem(
    @SerializedName("body")
    val body: String?=null,
    @SerializedName("id")
    val id: String?=null,
    @SerializedName("title")
    val title: String?=null,
    @SerializedName("userId")
    val userId: String?=null
):Serializable