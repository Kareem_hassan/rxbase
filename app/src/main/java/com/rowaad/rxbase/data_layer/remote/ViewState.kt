package com.rowaad.rxbase.data_layer.remote


sealed class ViewState<out T : Any>
class Success<out T : Any>(val data: T) : ViewState<T>()
class Fail<out T : Any>(val error: T, val errorCode: Int? = -1) : ViewState<T>()
class Loading<out T : Any>(val isVisible: Boolean) : ViewState<T>()
//class NoInternetState<T : Any> : ViewState<T>()