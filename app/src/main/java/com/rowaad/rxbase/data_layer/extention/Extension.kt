package com.rowaad.rxbase.data_layer.extention


import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import okhttp3.ResponseBody
import retrofit2.Response

inline fun <reified R> String.fromJson(): R = Gson().fromJson(this, object : TypeToken<R>() {}.type)

inline fun <reified R> R.toJson(): String = Gson().toJson(this, R::class.java)




