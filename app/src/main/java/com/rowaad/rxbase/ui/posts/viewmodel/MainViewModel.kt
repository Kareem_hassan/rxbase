package com.rowaad.rxbase.ui.posts.viewmodel

import com.rowaad.rxbase.data_layer.remote.Fail
import com.rowaad.rxbase.data_layer.remote.Success
import com.rowaad.rxbase.data_layer.repository.github.GitHubRepository
import com.rowaad.rxbase.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: GitHubRepository) : BaseViewModel() {

    fun getGitHubPosts(){
        repository.getPosts().subscribeWithLoading(onSuccess = {viewState.value=Success(it)},onErrorResponse = {viewState.value=Fail(it,0)},onGeneralError ={viewState.value=Fail(it)} )
    }
}