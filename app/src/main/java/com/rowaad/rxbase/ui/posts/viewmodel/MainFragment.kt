package com.rowaad.rxbase.ui.posts.viewmodel

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.kareem.rxbase.R
import com.rowaad.rxbase.data_layer.model.PostModel
import com.rowaad.rxbase.data_layer.remote.Fail
import com.rowaad.rxbase.data_layer.remote.Loading
import com.rowaad.rxbase.data_layer.remote.Success
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.ResponseBody

@AndroidEntryPoint
class MainFragment : Fragment() {

        private val authViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        authViewModel.getGitHubPosts()
        authViewModel.viewState.observe(viewLifecycleOwner, {state->
            when(state){
                is Loading ->{showProgress(state.isVisible)}
                is Success ->{showSuccess(state.data as PostModel)}
                is Fail ->{showError(state.error)}
            }
        })


    }

    private fun showSuccess(postModel: PostModel) {
        Log.v("data",postModel.toString())
    }

    private fun showError(error: Any) {
        if (error is Throwable) error.message?.let { Log.e("error", it) }
        else  handleErrorResponse(error as ResponseBody)
    }

    private fun handleErrorResponse(responseBody: ResponseBody) {
        Log.v("error-body",responseBody.string())
    }


    private fun showProgress(visible: Boolean) {
        Log.d("progress",visible.toString())
    }

}