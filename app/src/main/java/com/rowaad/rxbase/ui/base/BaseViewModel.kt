package com.rowaad.rxbase.ui.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rowaad.rxbase.data_layer.remote.Loading
import com.rowaad.rxbase.data_layer.remote.ViewState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response

open class BaseViewModel:ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    open val viewState = MutableLiveData<ViewState<Any>>()
    private fun addDisposable(disposable: Disposable) = compositeDisposable.add(disposable)

    private fun dispose() = compositeDisposable.dispose()


    fun <T> Observable<Response<T>>.subscribeWithLoading(
        onSuccess: (T) -> Unit,
        onErrorResponse: (throwable: ResponseBody) -> Unit,
        onGeneralError: (throwable: Throwable) -> Unit = { throw  it }) {
         subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe { viewState.value= Loading(true) }
        .doFinally{ viewState.value= Loading(false) }
        .subscribe({
            if (it.isSuccessful && it.body() != null)  onSuccess(it.body()!!) else  onErrorResponse(it.errorBody()!!)
        }, { onGeneralError(it) })
        .also { disposable-> addDisposable(disposable) }
    }


    protected fun <T> Single<Response<T>>.subscribeWithLoading(onSuccess: (T) -> Unit, onError: (throwable:Throwable) -> Unit = { throw  it}
    ,observeScheduler: Scheduler?=AndroidSchedulers.mainThread(),subscribeScheduler: Scheduler?=Schedulers.io()
    ) : Disposable {
        return subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
            .doOnSubscribe { viewState.value= Loading(true) }
            .doOnSuccess { it.body()?.let { it1 -> onSuccess(it1) } }
            .doOnError{ onError(it) }
            .doFinally{  viewState.value= Loading(false) }
            .subscribe()
    }

    override fun onCleared() {
        dispose()
        super.onCleared()
    }
}