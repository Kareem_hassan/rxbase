package com.rowaad.rxbase.di


import com.rowaad.rxbase.data_layer.repository.github.GitHubRepository
import com.rowaad.rxbase.data_layer.repository.github.GitHubRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class GitRepositoryModule {
    @Binds
    abstract fun providesGitHubRepo(gitHubRepository: GitHubRepositoryImpl): GitHubRepository

}