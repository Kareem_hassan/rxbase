package com.rowaad.rxbase.app

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.res.Configuration

import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    @SuppressLint("CheckResult")
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
    }
}